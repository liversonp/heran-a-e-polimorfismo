#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"
#include <iostream>
#include <string>

using namespace std;

class Quadrado: public FormaGeometrica{
public:	
	Quadrado();
	Quadrado(string tipo, float base, float altura);
	~Quadrado();

	float calcula_area();
    float calcula_perimetro();
};

#endif