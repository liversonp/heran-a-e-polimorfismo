#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"
#include <iostream>
#include <string>

using namespace std;

class Paralelogramo: public FormaGeometrica{
public:	
	Paralelogramo();
	Paralelogramo(string tipo, float base, float altura);
	~Paralelogramo();

	float calcula_area();
    float calcula_perimetro();
};

#endif