#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"
#include <iostream>
#include <string>

using namespace std;

class Circulo: public FormaGeometrica{
public:
	Circulo();
	Circulo(string tipo, float base, float altura);
	~Circulo();

	float calcula_area();
    float calcula_perimetro();
};

#endif