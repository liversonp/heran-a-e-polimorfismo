#include "triangulo.hpp"
#include <iostream>
#include <string> 

using namespace std;

Triangulo::Triangulo(string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

Triangulo::~Triangulo(){}

float Triangulo::calcula_area(){
	float calculo = (get_base()*get_altura())/2;
	return calculo;
}

float Triangulo::calcula_perimetro(){
    float calculo = get_base()*get_base()*get_base();
	return calculo;
}