#include "pentagono.hpp"
#include <iostream>
#include <string> 

using namespace std;

Pentagono::Pentagono(string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

Pentagono::~Pentagono(){}	

float Pentagono::calcula_area(){
	float calculo = ((calcula_perimetro() * (get_altura()/2))/2);
	return calculo;
}

float Pentagono::calcula_perimetro(){
   	float calculo = 5*(get_base());
	return calculo;
}