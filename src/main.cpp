#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main(){
	vector <FormaGeometrica*> formas;
	float base,altura;

	cout<< "Digite a base e a altura desejados:\n";
	cin>>base>>altura;

	formas.push_back(new Triangulo("Triangulo",base,altura));
	formas.push_back(new Quadrado("Quadrado",base,altura));
	formas.push_back(new Paralelogramo("Paralelogramo",base,altura));
	formas.push_back(new Pentagono("Pentagono",base,altura));
	formas.push_back(new Hexagono("Hexagono",base,altura));
	formas.push_back(new Circulo("Circulo",base,altura));

		for(FormaGeometrica *p : formas){
			cout<<"Tipo: " << p->get_tipo()<<endl;
			cout<<"Calculo área: "<< p->calcula_area()<<" | Calculo perimetro: "<< p->calcula_perimetro()<<endl;
		}

		cout<<endl;

		for(FormaGeometrica *p : formas){
			delete p;
		}

		while(!formas.empty())
			formas.pop_back();
	return 0;
}