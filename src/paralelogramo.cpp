#include "paralelogramo.hpp"
#include <iostream>
#include <string> 

using namespace std;

Paralelogramo::Paralelogramo(string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

Paralelogramo::~Paralelogramo(){}

float Paralelogramo::calcula_area(){
	float calculo = (get_base()*get_altura());
	return calculo;
}

float Paralelogramo::calcula_perimetro(){
   	float calculo = 2*(get_base() + get_altura());
	return calculo;
}