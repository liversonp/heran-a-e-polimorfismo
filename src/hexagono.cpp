#include "hexagono.hpp"
#include <iostream>
#include <string> 

using namespace std;

Hexagono::Hexagono(string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

Hexagono::~Hexagono(){}

float Hexagono::calcula_area(){
	float calculo = 6*((get_base()*(get_altura()/2))/2);
	return calculo;
}

float Hexagono::calcula_perimetro(){
   	float calculo = 6*(get_base());
	return calculo;
}