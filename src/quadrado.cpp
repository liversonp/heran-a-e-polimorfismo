#include "quadrado.hpp"
#include <iostream>
#include <string> 

using namespace std;

Quadrado::Quadrado(string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

Quadrado::~Quadrado(){}

float Quadrado::calcula_area(){
	float calculo = (get_base()*get_base());
	return calculo;
}

float Quadrado::calcula_perimetro(){
   	float calculo = (2*get_base()) + (2*get_altura());
	return calculo;
}