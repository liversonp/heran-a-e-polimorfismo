#include "circulo.hpp"
#include <iostream>
#include <string>

using namespace std;

Circulo::Circulo(string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

Circulo::~Circulo(){}

float Circulo::calcula_area(){
	float calculo = 3.1415926*(get_altura()/2)*(get_altura()/2);
	return calculo;
}

float Circulo::calcula_perimetro(){
	float calculo = 2*3.1415926*(get_altura()/2);
	return calculo;
}